import unittest
import socket
import sys

sys.path.append("Protocols/")
import Mobile_Server_Protocol_pb2 as Mobile_Server

from configuration import config

import utils


class TestDbControllerInstance(unittest.TestCase):

    def setUp(self):
        self.server_socket = socket.socket()
        self.server_socket.connect(('localhost', config.db_controller_address()[1]))

    def tearDown(self):
        self.server_socket.close()

    def _build_char_rate_req(self, token, charIndex, valid):
        req = Mobile_Server.CharacterRateRequest()
        req.token = token
        req.characterIndex = charIndex
        req.valid = valid
        print ("Request {}, {}, {}".format(token, charIndex, valid))
        return req

    def serialize_resp(self, data):
        resp = Mobile_Server.CharacterRateResponse()
        resp.ParseFromString(data)
        return resp

    def check_resp(self, resp):
        print("Resp {}".format(resp.success))
        self.assertTrue(resp.success)

    def test_preprocessing_req_resp(self):
        #build req
        token = "some_token"
        charIndex = 42
        valid = True
        req = self._build_char_rate_req(token, charIndex, valid)
        # send it
        utils.send(self.server_socket, req.SerializeToString())
        # receive resp
        data = utils.receive(self.server_socket)
        resp = self.serialize_resp(data)
        # #check
        self.check_resp(resp)


if __name__ == '__main__':
    unittest.main()