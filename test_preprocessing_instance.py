import unittest
import socket
import sys

sys.path.append("Protocols/")
import Server_Preprocessing_Protocol_pb2 as Server_Preprocessing

from configuration import config

import utils

class TestPreprocessingInstance(unittest.TestCase):

    def setUp(self):
        self.server_socket = socket.socket()
        self.server_socket.connect(('localhost', config.preprocessing_address()[1]))

    def tearDown(self):
        self.server_socket.close()

    def _build_preprocessing_req(self, token, img_path, save_path):
        req = Server_Preprocessing.PreprocessingRequest()
        req.token = token
        req.imagePath = img_path
        req.recognitionRectangle.x1 = 100
        req.recognitionRectangle.x2 = 1550
        req.recognitionRectangle.y1 = 80
        req.recognitionRectangle.y2 = 650
        req.savePath = save_path

        return req

    def serialize_resp(self, data):
        resp = Server_Preprocessing.PreprocessingResponse()
        resp.ParseFromString(data)
        return resp

    def check_resp(self, to_check, resp):
        token,  = to_check
        self.assertEqual(token, resp.token)

    def test_preprocessing_req_resp(self):
        #build req
        token = "some_token"
        img_path = "image2.jpg"
        save_path = "."
        req = self._build_preprocessing_req(token, img_path, save_path)
        # send it
        utils.send(self.server_socket, req.SerializeToString())
        # receive resp
        data = utils.receive(self.server_socket)
        resp = self.serialize_resp(data)
        #check
        to_check = (token,)
        self.check_resp(to_check, resp)


if __name__ == '__main__':
    unittest.main()