import logger
from server_connector import ServerConnector
from recognition_server_message_handler import RecognitionServerMessageHandler

from configuration import config
class RecognitionInstance:

    def __init__(self, host, port):
        self.log = logger.get_logger(self.__class__.__name__ + '_{}'.format(port))
        self.log.info("===== STARTING RECOGNITION INSTANCE =====")

        self.server_connector = ServerConnector((host, port))

        self.recognition_server_message_handler = RecognitionServerMessageHandler(
            server_connector=self.server_connector,
        )


if __name__ == '__main__':
    RecognitionInstance(*config.recognition_address())
