import socket
import sys
import unittest
import hashlib
sys.path.append("Protocols/")
import Mobile_Server_Protocol_pb2 as Mobile_Server
import Server_Preprocessing_Protocol_pb2 as Server_Preprocessing
import Server_Recognition_Protocol_pb2 as Server_Recogniotion
import common_pb2 as common
from threading import Event
from configuration import config

from utils import send, receive

RECOGNIZED_CHARS_COUNT = 5


class TestMobileConnection(unittest.TestCase):
    def setUp(self):
        self.preprocessing_thread_ended_event = Event()
        self.recognition_thread_ended_event = Event()
        self.mobile_socket = socket.socket()
        self.mobile_socket.connect(("localhost", config.server_address()[1]))
        # self.mobile_socket.connect(("195.68.233.248", config.server_address()[1]))
        self.current_index = 0
        self.token = None

    def tearDown(self):
        self.mobile_socket.close()

    def init_sequence(self):
        cmd = Mobile_Server.Command()
        cmd.initRequest.CopyFrom(Mobile_Server.InitRequest())
        send(self.mobile_socket, cmd.SerializeToString())
        data = receive(self.mobile_socket)
        print("Received init resp")
        response = Mobile_Server.Command()
        response.ParseFromString(data)
        self.token = response.initResponse.token
        self.assertTrue(response.initResponse.success)
        # self.assertEqual(response.initResponse.token, "some_token")

    def image_sequence(self):
        cmd = Mobile_Server.Command()
        cmd.image.CopyFrom(self.build_image_message())
        data = cmd.SerializeToString()
        send(self.mobile_socket, data)
        print("Sent image req")
        data = receive(self.mobile_socket)
        response = Mobile_Server.Command()
        response.ParseFromString(data)
        self.assertTrue(response.imageResponse.success)
        data = receive(self.mobile_socket)
        cmd = Mobile_Server.Command()
        cmd.ParseFromString(data)
        self.check_recog_result(cmd.recognitionResult)


    def check_recog_result(self, result):
        for idx, char in enumerate(result.character):
            self.assertEqual(char.index, idx)
            print("Recognized char : {}, certainty : {}"
                  .format(
                char.character,
                char.certainty
            ))
            # self.assertEqual(result.character[i].character, 'a')
            # self.assertEqual(result.character[i].certainty, 42)
            # self.assertEqual(result.character[i].characterRectangle.x1, 1)
            # self.assertEqual(result.character[i].characterRectangle.x2, 2)
            # self.assertEqual(result.character[i].characterRectangle.y1, 3)
            # self.assertEqual(result.character[i].characterRectangle.y2, 4)



    def build_image_message(self):
        msg = Mobile_Server.Image()
        msg.image = self.read_file("image.jpg")
        msg.token = self.token
        msg.imageSize = 42
        msg.imageFormat = "jpg"
        msg.hashCode = self.create_hex_hash_string(msg.image)
        rect = common.Rectangle()
        rect.x1 = 100
        rect.x2 = 2000
        rect.y1 = 80
        rect.y2 = 2000
        msg.recognitionRectangle.CopyFrom(rect)
        return msg

    def create_hex_hash_string(self, data):
        m = hashlib.new("md5")
        m.update(data)
        return m.hexdigest()


    def read_file(self, filename):
        with open(filename, "rb") as f:
            img = f.read()
        return img

    def build_translate_req(self, text, target):
        req = Mobile_Server.TranslateRequest()
        req.text = text
        req.targetLanguage = target
        # print ("Request {}, {}".format(text, target))
        return req
    
    def build_char_rate_req(self, token, charIndex, valid):
        req = Mobile_Server.CharacterRateRequest()
        req.token = token
        req.characterIndex = charIndex
        req.valid = valid
        return req

    def check_translation_resp(self, resp, translation):
        self.assertEqual(resp.success, True)
        self.assertEqual(resp.translatedText, translation)
    
    def check_char_rate(self, resp, success):
        if success:
            self.assertTrue(resp)
        else:
            self.assertFalse(resp)

    def recog_sequence(self):
        self.init_sequence()
        self.image_sequence()


    def char_rate_sequence(self):
        token = "some_token"
        characterIndex = 123456
        valid = True
        cmd = Mobile_Server.Command()
        req = self.build_char_rate_req(token, characterIndex, valid)
        cmd.characterRateRequest.CopyFrom(req)
        data = cmd.SerializeToString()
        send(self.mobile_socket, data)
        data = receive(self.mobile_socket)
        response = Mobile_Server.Command()
        response.ParseFromString(data)
        self.check_char_rate(response.characterRateResponse, True)

    def translate_sequence(self):
        text = u"Mam pięknego kota"
        target = "en"
        translation = u"I have a beautiful cat"
        cmd = Mobile_Server.Command()
        req = self.build_translate_req(text, target)
        cmd.translateRequest.CopyFrom(req)
        data = cmd.SerializeToString()
        send(self.mobile_socket, data)
        data = receive(self.mobile_socket)
        response = Mobile_Server.Command()
        response.ParseFromString(data)
        self.check_translation_resp(response.translateResponse, translation)

    def test_translate_sequence(self):
        self.translate_sequence()

    def test_recog_sequence(self):
        for i in range(1):
            self.recog_sequence()

    def test_char_rate_sequence(self):
        self.char_rate_sequence()
        


if __name__ == '__main__':
    unittest.main()
