import logger
from configuration import config
import threading
import sys
sys.path.append("Protocols/")
import Mobile_Server_Protocol_pb2 as Mobile_Server


class DBControllerServerMessageHandler:

    def __init__(self, server_connector):
        self.log = logger.get_logger(
            self.__class__.__name__ + "_{}".format(config.db_controller_address()[1])
        )


        self.server_connector = server_connector
        self.main_loop = threading.Thread(target=self._main_loop)
        self.main_loop.start()

    def _main_loop(self):
        self.log.info("Main loop started")
        while True:
            self._handle_char_rate_req(self.server_connector.receive())

    def add_to_db(self, args):
        self.log.info("Adding to db : {}".format(args))
        return True

    def build_response(self, result):
        resp = Mobile_Server.CharacterRateResponse()
        resp.success = result
        return resp

    def _handle_char_rate_req(self, args):
        conn, addr, data = args

        req = Mobile_Server.CharacterRateRequest()
        req.ParseFromString(data)

        self.log.info("Handling req; PARAMS:"
                      " token ({}), characterIndex({})"
                      "valid ({})"
                      "".format(req.token, req.characterIndex, req.valid))

        try:
            result = self.add_to_db((req.token, req.characterIndex, req.valid))
            resp = self.build_response(result)
            self.log.info("Resp: success({})".format(resp.success))
            self.server_connector.send(conn, addr, resp)
        except Exception as e:
            self.log.warn("Exception {}".format(e))
            resp = self.build_response(False)
            self.server_connector.send(conn, addr, resp)
            raise e





