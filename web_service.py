from flask import Flask, render_template, send_from_directory
import flask.logging
import os
import logger
import logging

app = Flask(__name__,
            static_url_path='/data')

def read_file_to_list(directory, filename):
    with open("./{}/{}".format(directory, filename)) as file:
        return file.read().split('\n')

@app.route('/')
def main_page():
    return render_template(
        'main_page.html',
        options=[
            ('logs', 'Logs'),
            ('data', 'Images')
        ]
    )

@app.route('/logs')
def logs():
    return render_template(
        'dir.html',
        title="Logs",
        directory="logs",
        dir_list=os.listdir('./logs')
    )

@app.route('/logs/<filename>')
def logs_file(filename):
    return render_template(
        'log_file.html',
        filename=filename,
        log_file_lines=read_file_to_list('logs', filename)
    )

@app.route('/data')
def data():
    return render_template(
        'dir.html',
        title="Images",
        directory="data",
        dir_list=os.listdir('./data'))

@app.route('/data/<filename>')
def data_file(filename):
    return send_from_directory('./data/', filename)


def main():
    app.run(
        host='0.0.0.0',
        port=5000
    )


if __name__ == '__main__':
    main()