from multiprocessing import Queue
import logger
import socket
import threading
import utils
from configuration import config
import os


class DbControllerConnector:
    def __init__(self):
        self.log = logger.get_logger(self.__class__.__name__)
        self.db_controller_output_queue = Queue()
        self.db_controller_input_queue = Queue()
        
        self.db_controller_output_thread = threading.Thread(target=self._db_controller_sending_thread)
        self.db_controller_output_thread.start()
    
    def _db_controller_sending_thread(self):
        self.log.info("Starting db controller sending thread")
        db_controller_address = os.getenv('DB_CONTROLLER_SERVICE_HOST', 'localhost'), config.db_controller_address()[1]
        self.log.info("Target address  = {}:{}".format(*db_controller_address))
        while True:
            data = self.db_controller_output_queue.get()
            sock = socket.socket()
            sock.connect(db_controller_address)
            self.log.info("Connected to db controller on {}:{}".format(*db_controller_address))
            self.log.info("Sending data to {}:{}".format(*db_controller_address))
            utils.send(sock, data)
            resp = utils.receive(sock)
            self.db_controller_input_queue.put(resp)
            sock.close()
    
    def send(self, proto_object):
        self.log.info("Sending CharacterRateRequest")
        self.db_controller_output_queue.put(proto_object.SerializeToString())
    
    def receive(self):
        self.log.info("Waiting for data from DbController")
        data = self.db_controller_input_queue.get()
        self.log.info("Received data from DbController")
        return data



