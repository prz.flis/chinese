import logger
import sys
import threading
import utils
sys.path.append("Protocols/")

import Server_Preprocessing_Protocol_pb2 as Server_Preprocessing
from configuration import config
if config.use_real_modules():
    sys.path.append("externals/preprocessing_chinese_char_recog/PythonApplication1")
    
    from ImageAnalyser import ImageAnalyser




class PreprocessingServerMessageHandler:
    def __init__(self, server_connector):
        self.log = logger.get_logger(
            self.__class__.__name__ + "_{}".format(config.preprocessing_address()[1])
        )
        if config.use_real_modules():
            self.preprocessor = ImageAnalyser()

        self.server_connector = server_connector

        self.main_loop_thread = threading.Thread(target=self._main_loop)
        self.main_loop_thread.start()

    def _main_loop(self):
        while True:
            self._handle_preprocessing_request(self.server_connector.receive())

    def _handle_preprocessing_request(self, args):
        conn, addr, data = args

        req = Server_Preprocessing.PreprocessingRequest()
        req.ParseFromString(data)
        self.log.info("Handling request with token:  {}".format(req.token))

        filename = req.token + ".jpg"
        image_save_path = config.image_path() + filename
        utils.save_to_file(req.imageBinary, image_save_path)
        resp = Server_Preprocessing.PreprocessingResponse()

        if config.use_real_modules():
            self._do_real_preprocessing(req, resp, image_save_path)
        else:
            self._do_fake_preprocessing(req, resp, image_save_path)

        self.server_connector.send(conn, addr, resp)

    def _do_real_preprocessing(self, req, resp, image_path):
        resp.token = req.token
        images_list =  self.preprocessor.detectPotentialChineseCharacters(
            inputImageFilename=image_path,
                outputImageDirectory=req.savePath,
                outputImagesPrefix="{}_".format(req.token),
                x1=req.recognitionRectangle.x1,
                x2=req.recognitionRectangle.x2,
                y1=req.recognitionRectangle.y1,
                y2=req.recognitionRectangle.y2,
                saveAsBinary=True
        )
        i=0
        for image in images_list:
            char = resp.preprocessedCharacterList.add()
            char.index = i
            i+=1
            # char.characterNumpyObjectPath = image[0]
            char.characterNumpyObjectBinary = utils.read_file_to_binary(image[0])
            char.characterImageBinary = utils.read_file_to_binary(image[0])
            self.log.info(f"LEN = {len(char.characterNumpyObjectBinary)}")
            char.rectangle.x1 = image[1][0]
            char.rectangle.y1 = image[1][1]
            char.rectangle.x2 = image[2][0]
            char.rectangle.y2 = image[2][1]
            self.log.info("Possible character  {}\t{}".format(
                # char.characterNumpyObjectPath,
                # char.characterImagePath,
                (char.rectangle.x1, char.rectangle.y1),
                (char.rectangle.x2, char.rectangle.x2),
            ))

    def _do_fake_preprocessing(self, req, resp):
        resp.token = req.token
        for i in range(5):
            char = resp.preprocessedCharacterList.add()
            char.index = i
            numpy_path = "numpy_path{}".format(i)
            image_path = "image_path{}".format(i)
            char.characterNumpyObjectPath = numpy_path
            char.characterImagePath = image_path
            char.rectangle.x1 = 100 + i*100
            char.rectangle.x2 = 200 + i*100
            char.rectangle.y1 = 300 + i*100
            char.rectangle.y2 = 400 + i*100
            self.log.info("Possible character  {}\t{}\t{}\t{}".format(
                numpy_path,
                image_path,
                (char.rectangle.x1, char.rectangle.y1),
                (char.rectangle.x2, char.rectangle.x2),
            ))
