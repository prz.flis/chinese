import unittest
import socket
import sys

sys.path.append("Protocols/")
import Mobile_Server_Protocol_pb2 as Mobile_Server

from configuration import config

import utils


class TestTranslatingInstance(unittest.TestCase):

    def setUp(self):
        self.server_socket = socket.socket()
        self.server_socket.connect(('localhost', config.translate_address()[1]))

    def tearDown(self):
        self.server_socket.close()

    def _build_translate_req(self, text, target):
        req = Mobile_Server.TranslateRequest()
        req.text = text
        req.targetLanguage = target
        print ("Request {}, {}".format(text, target))
        return req

    def serialize_resp(self, data):
        resp = Mobile_Server.TranslateResponse()
        resp.ParseFromString(data)
        return resp

    def check_resp(self, resp):
        print("Resp {}; {}".format(resp.success, resp.translatedText))
        # self.assertEqual(token, resp.token)

    def test_preprocessing_req_resp(self):
        #build req
        text = u"Jestem bardzo mądra"
        target = "es"
        req = self._build_translate_req(text, target)
        # send it
        utils.send(self.server_socket, req.SerializeToString())
        # receive resp
        data = utils.receive(self.server_socket)
        resp = self.serialize_resp(data)
        # #check
        self.check_resp(resp)


if __name__ == '__main__':
    unittest.main()