import socket
import sys
import unittest
import threading
from configuration import config
import hashlib

sys.path.append("Protocols/")
import Mobile_Server_Protocol_pb2 as Mobile_Server
import Server_Preprocessing_Protocol_pb2 as Server_Preprocessing
import Server_Recognition_Protocol_pb2 as Server_Recogniotion
import common_pb2 as common

RECOGNIZED_CHARS_COUNT=3

import utils

class TestMobileConnection(unittest.TestCase):
    
    def setUp(self):
        self.mobile_socket = socket.socket()
        self.mobile_socket.connect(("localhost", config.server_address()[1]))
        self.current_index = 0
        self.token = None
    
    def tearDown(self):
        self.mobile_socket.close()
        
    def init_sequence(self):
        cmd = Mobile_Server.Command()
        cmd.initRequest.CopyFrom(Mobile_Server.InitRequest())
        utils.send(self.mobile_socket, cmd.SerializeToString())
        # self.mobile_socket.send(cmd.SerializeToString())
        data = utils.receive(self.mobile_socket)
        # data = self.mobile_socket.recv(1024)
        response = Mobile_Server.Command()
        response.ParseFromString(data)
        self.assertTrue(response.initResponse.success)
        self.token = response.initResponse.token

    def image_sequence(self):
        preprocessing_thread = threading.Thread(target=self.preprocessing_thread)
        preprocessing_thread.start()
        recognition_thread = threading.Thread(target=self.recognition_thread)
        recognition_thread.start()
        
        cmd = Mobile_Server.Command()
        cmd.image.CopyFrom(self.build_image_message())
        utils.send(self.mobile_socket, cmd.SerializeToString())
        data = utils.receive(self.mobile_socket)
        response = Mobile_Server.Command()
        response.ParseFromString(data)
        self.assertTrue(response.imageResponse.success)
        data = self.mobile_socket.recv(10240)
        cmd = Mobile_Server.Command()
        cmd.ParseFromString(data)
        self.check_recog_result(cmd.recognitionResult)
        
        preprocessing_thread.join()
        recognition_thread.join()
        
        
    def preprocessing_thread(self):
        preprocessing_socket = socket.socket()
        preprocessing_socket.bind(config.preprocessing_address())
        preprocessing_socket.listen(1)
        conn, addr = preprocessing_socket.accept()
        data = conn.recv(10240)
        req = Server_Preprocessing.PreprocessingRequest()
        req.ParseFromString(data)
        self.check_preprocessing_req(req)
        conn.send(self.build_preprocessing_response().SerializeToString())
        conn.close()
        preprocessing_socket.close()
    
    def recognition_thread(self):
        address = config.recognition_address()
        for i in range(RECOGNIZED_CHARS_COUNT):
            recognition_socket = socket.socket()
            recognition_socket.bind(address)
            recognition_socket.listen(1)
            conn, addr = recognition_socket.accept()
            data = conn.recv(10240)
            req = Server_Recogniotion.RecognitionRequest()
            req.ParseFromString(data)
            self.check_recognition_request(req, i)
            conn.send(self.build_recognition_response().SerializeToString())
            conn.close()
            recognition_socket.close()
    
    def check_recognition_request(self, req, index):
        self.assertEqual(req.token, self.token)
        self.assertEqual(req.preprocessedCharacter.index, index)
        self.assertEqual(req.preprocessedCharacter.characterNumpyObjectPath, "asd")
        self.assertEqual(req.preprocessedCharacter.characterImagePath, "asd")
        self.assertEqual(req.preprocessedCharacter.rectangle.x1, 1)
        self.assertEqual(req.preprocessedCharacter.rectangle.x2, 2)
        self.assertEqual(req.preprocessedCharacter.rectangle.y1, 3)
        self.assertEqual(req.preprocessedCharacter.rectangle.y2, 4)

    def check_preprocessing_req(self, req):
        self.assertEqual(req.token, self.token)
        self.assertEqual(req.recognitionRectangle.x1, 1)
        self.assertEqual(req.recognitionRectangle.x2, 2)
        self.assertEqual(req.recognitionRectangle.y1, 3)
        self.assertEqual(req.recognitionRectangle.y2, 4)
    
    def check_recog_result(self, result):
        for i in range(RECOGNIZED_CHARS_COUNT):
            self.assertEqual(result.character[i].index, i)
            self.assertEqual(result.character[i].character, 'a')
            self.assertEqual(result.character[i].certainty, 42)
            self.assertEqual(result.character[i].characterRectangle.x1, 1)
            self.assertEqual(result.character[i].characterRectangle.x2, 2)
            self.assertEqual(result.character[i].characterRectangle.y1, 3)
            self.assertEqual(result.character[i].characterRectangle.y2, 4)

    def build_recognition_response(self):
        msg = Server_Recogniotion.RecognitionResponse()
        msg.token = self.token
        recog_char = common.RecognizedCharacter()
        recog_char.index = self.current_index
        self.current_index += 1
        recog_char.character = 'a'
        recog_char.certainty = 42
        msg.recognizedCharacter.CopyFrom(recog_char)
        return msg
    
    def create_hex_hash_string(self, data):
        m = hashlib.new("md5")
        m.update(data)
        return m.hexdigest()
    
    def build_image_message(self):
        msg = Mobile_Server.Image()
        msg.token = self.token
        msg.imageSize = 42
        msg.imageFormat = "png"
        msg.image = b'123'
        msg.hashCode = self.create_hex_hash_string(msg.image)
        rect = common.Rectangle()
        rect.x1 = 1
        rect.x2 = 2
        rect.y1 = 3
        rect.y2 = 4
        msg.recognitionRectangle.CopyFrom(rect)
        
        return msg
    
    def build_preprocessing_response(self):
        msg = Server_Preprocessing.PreprocessingResponse()
        msg.token = "some_token"
        for i in range(RECOGNIZED_CHARS_COUNT):
            prep_char = msg.preprocessedCharacterList.add()
            prep_char.index = i
            prep_char.characterNumpyObjectPath = "asd"
            prep_char.characterImagePath = "asd"
            rect = common.Rectangle()
            rect.x1 = 1
            rect.x2 = 2
            rect.y1 = 3
            rect.y2 = 4
            prep_char.rectangle.CopyFrom(rect)
        return msg
    
    

        
        
        
        
        
    
    # def test_translate_sequence(self):
    #     self.translate_sequence()
    #
    #
    def test_recog_sequence(self):
        self.init_sequence()
        self.image_sequence()

        
if __name__ == '__main__':
    unittest.main()
