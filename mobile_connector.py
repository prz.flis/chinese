from multiprocessing import Queue
import logger
import threading
import utils

class MobileConnector:
    def __init__(self, endpoint):
        self.log = logger.get_logger(self.__class__.__name__)
        self.endpoint_to_mobile = endpoint
        
        self.mobile_output_queue = Queue()
        self.mobile_input_queue = Queue()
        
        self.mobile_output_thread = threading.Thread(target=self._mobile_sending_thread)
        self.mobile_output_thread.start()
        
        self.connection_with_mobile_thread = threading.Thread(target=self._connection_with_mobile)
        self.connection_with_mobile_thread.start()
    
    def _mobile_sending_thread(self):
        self.log.info("Starting mobile sending thread")
        while True:
            conn, addr, data = self.mobile_output_queue.get()
            self.log.info("Sending data to {}:{}".format(*addr))
            utils.send(conn, data)
    
    def send(self, conn, addr, command):
        self.log.info("Sending {}".format(command.WhichOneof('type')))
        self.mobile_output_queue.put((conn, addr, command.SerializeToString()))
    
    def receive(self):
        self.log.info("Waiting for data from mobile")
        data = self.mobile_input_queue.get()
        self.log.info("Received data from mobile")
        return data
        
    def _connection_with_mobile(self):
        while True:
            #TODO:  accept multiple clients parallel
            conn, addr = self.endpoint_to_mobile.accept()
            self.log.info('Connected with {}:{}'.format(*addr))
            while True:
                data = utils.receive(conn)
                if data:
                    self.mobile_input_queue.put((conn, addr, data))
                else:
                    self.log.info('Disconnected with {}:{}'.format(*addr))
                    break