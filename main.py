import sys
import socket

import logger
from configuration import config

sys.path.append("Protocols/")
from server_mobile_message_handler import ServerMobileMessageHandler
from preprocessing_connector import PreprocessingConnector
from translate_connector import TranslateConnector
from recognition_connector import RecognitionConnector
from mobile_connector import MobileConnector
from traffic_router import TrafficRouter
from db_controller_connector import DbControllerConnector


# TODO: storing data in DbController

# TODO: use real modules

# TODO: logs cant be seen on docker vm
# TODO: config based on xml
# TODO: graceful shutdown
# TODO: save in directory given in savepath request parameter
# TODO: introduce  server_recognition & server_preprocessing message handlers
# TODO: remove _connection_waiting_loop
# TODO: stubs instead of recognition thread in tests
# TODO: add exception handling (parsing, socket etc)



class Server:
    def __init__(self):
        self.log = logger.get_logger(self.__class__.__name__)

        self.preprocessing_connector = PreprocessingConnector()
        # self.recognition_connector = RecognitionConnector(("localhost", config.recognition_address()[1]))
        
        self.translating_connector = TranslateConnector()

        self.db_controller_connector = DbControllerConnector()

        self.recognition_traffic_router = TrafficRouter(config.number_of_recognition_instances())

        self.mobile_connector = MobileConnector(
            endpoint=self.setup_endpoint_to_mobile(),
        )
        self.server_mobile_message_handler = ServerMobileMessageHandler(
            self.preprocessing_connector,
            self.recognition_traffic_router,
            self.mobile_connector,
            self.translating_connector,
            self.db_controller_connector
        )

    def setup_endpoint_to_mobile(self):
        soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.log.info("Socket Created")
        try:
            soc.bind(config.server_address())
            self.log.info("Socket bind complete")
            self.log.info(repr(soc.getsockname()))
        except socket.error as msg:
            self.log.error("Cannot bind socket")
            self.log.error(msg)
        soc.listen(4)
        return soc

if __name__ == '__main__':
    Server()
