import logger
from configuration import config
import threading
import os
import sys
import json
sys.path.append("Protocols/")
import Mobile_Server_Protocol_pb2 as Mobile_Server

from apiclient.discovery import build

# from google.cloud import translate

class TranslatingServerMessageHandler:

    def __init__(self, server_connector):
        self.log = logger.get_logger(
            self.__class__.__name__ + "_{}".format(config.translate_address()[1])
        )


        self.collection = build('translate', 'v2', developerKey="AIzaSyCOfmPFYst7SinXqTTmKkqltkOtQRHiPzs")\
            .translations()

        self.server_connector = server_connector
        self.main_loop = threading.Thread(target=self._main_loop)
        self.main_loop.start()

    def _main_loop(self):
        self.log.info("Main loop started")
        while True:
            self._handle_translate_request(self.server_connector.receive())


    def do_translation(self, text, target):
        req = self.collection.list(q=text, target=target)
        resp = req.execute()
        return [trans['translatedText'] for trans in resp['translations']]

    def build_response(self, result, text):
        resp = Mobile_Server.TranslateResponse()
        resp.success = result
        resp.translatedText = text

        return resp

    def _handle_translate_request(self, args):
        conn, addr, data = args

        req = Mobile_Server.TranslateRequest()
        req.ParseFromString(data)

        # self.log.info(u"Handling req; PARAMS:"
        #               u" string ({}), target({})"
        #               u"".format(req.text, req.targetLanguage))

        try:
            translations = self.do_translation(req.text, req.targetLanguage)
            text = u"\n".join(translations)
            # self.log.info("Translation: {}".format(text))
            resp = self.build_response(True, text)
            self.server_connector.send(conn, addr, resp)
        except Exception as e:
            self.log.warn("Exception {}".format(e))
            resp = self.build_response(False, "")
            self.server_connector.send(conn, addr, resp)





