from multiprocessing import Queue
import logger
import socket
import threading
import utils
from configuration import config
import os


class TranslateConnector:
    def __init__(self):
        self.log = logger.get_logger(self.__class__.__name__)
        self.translating_output_queue = Queue()
        self.translating_input_queue = Queue()
        
        self.translating_output_thread = threading.Thread(target=self._translating_sending_thread)
        self.translating_output_thread.start()
    
    def _translating_sending_thread(self):
        self.log.info("Starting translating sending thread")
        translating_address = os.getenv('TRANSLATING_SERVICE_HOST', 'localhost'), config.translate_address()[1]
        self.log.info("Target address  = {}:{}".format(*translating_address))
        while True:
            data = self.translating_output_queue.get()
            sock = socket.socket()
            sock.connect(translating_address)
            self.log.info("Connected to translating on {}:{}".format(*translating_address))
            self.log.info("Sending data to {}:{}".format(*translating_address))
            utils.send(sock, data)
            resp = utils.receive(sock)
            self.translating_input_queue.put(resp)
            sock.close()
    
    def send(self, proto_object):
        self.log.info("Sending TranslateRequest")
        self.translating_output_queue.put(proto_object.SerializeToString())
    
    def receive(self):
        self.log.info("Waiting for data from Translating")
        data = self.translating_input_queue.get()
        self.log.info("Received data from Translating")
        return data



