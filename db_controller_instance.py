import logger
from configuration import config
from server_connector import ServerConnector
from db_controller_message_handler import DBControllerServerMessageHandler
import os

class DbControllerInstance:

    def __init__(self):
        host, port = config.db_controller_address()
        self.log = logger.get_logger(self.__class__.__name__ + '_{}'.format(port))
        self.log.info("===== STARTING DB CONTROLLER INSTANCE =====")

        self.server_connector = ServerConnector((os.getenv("DB_CONTROLLER_SERVICE_HOST", host), port))
        self.db_controller_message_handler = DBControllerServerMessageHandler(
            server_connector=self.server_connector,
        )


if __name__ == '__main__':
    DbControllerInstance()