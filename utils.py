import struct

"""

|   length = N              |       proto_msg       |
    4 bytes  Big Endian             N bytes

"""

def receive(sock):
    length_bytes = sock.recv(4)
    if not length_bytes:
        return ''
    length,  = struct.unpack(">I", length_bytes)
    data = b''
    while length > 0:
        buffer = sock.recv(length)
        length -= len(buffer)
        data += buffer
    return data

def send(sock, data):
    length = len(data)
    length_bytes = struct.pack(">I", length)
    to_send = length_bytes + data
    sock.send(to_send)

def save_to_file( binary, filename):
    with open(filename, "wb") as f:
        f.write(binary)

def read_file_to_binary(filepath):
    with open(filepath, "rb") as f:
        return f.read()