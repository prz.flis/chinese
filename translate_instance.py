import logger
from configuration import config
from server_connector import ServerConnector
from translating_server_message_handler import TranslatingServerMessageHandler
import os


class TranslateInstance:

    def __init__(self):
        host, port = config.translate_address()
        self.log = logger.get_logger(self.__class__.__name__ + '_{}'.format(port))
        self.log.info("===== STARTING TRANSLATING INSTANCE =====")

        self.server_connector = ServerConnector((os.getenv("TRANSLATING_SERVICE_HOST", host), port))
        self.translating_server_message_handler = TranslatingServerMessageHandler(
            server_connector=self.server_connector,
        )


if __name__ == '__main__':
    TranslateInstance()