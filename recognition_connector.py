from multiprocessing import Queue
import threading
import logger
import socket
import time
import utils

from configuration import config

class RecognitionConnector:
    
    def __init__(self, recognition_address):

        self.log = logger.get_logger(self.__class__.__name__)
        self.recognition_output_queue = Queue()
        self.recognition_input_queue = Queue()
        self.recognition_address = recognition_address
        self.recognition_output_thead = threading.Thread(target=self._recognition_connection_thread)
        self.recognition_output_thead.start()


    def _recognition_connection_thread(self):
        self.log.info("Starting recognition sending thread")
        self.log.info("Target address  = {}:{}".format(*self.recognition_address))
        while True:
            data = self.recognition_output_queue.get()
            soc = socket.socket()
            self._connection_waiting_loop(soc, self.recognition_address)
            self.log.info("Connected to recognition on {}:{}".format(*self.recognition_address))
            self.log.info("Sending data to {}:{}".format(*self.recognition_address))
            utils.send(soc, data)
            resp = utils.receive(soc)
            self.log.info("Received data from recognition")
            soc.close()
            self.recognition_input_queue.put(resp)

    def _connection_waiting_loop(self, soc, address, timeout=10):
        while timeout > 0:
            try:
                soc.connect(address)
                return
            except socket.error as msg:
                self.log.error("Cannot connect to {}:{}, time left={}({})".format(*address, timeout, msg))
                time.sleep(1)
                timeout -= 1
        raise socket.error("recognition_connection_exception")
        
    
    def send(self, data):
        self.log.info("Sending RecognitionRequest")
        self.recognition_output_queue.put(data)
    
    def receive(self):
        self.log.info("Waiting for data from Recognition")
        data = self.recognition_input_queue.get()
        return data
    
