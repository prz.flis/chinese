from multiprocessing import Queue
import logger
import socket
import threading
import utils
from configuration import config
import os


class PreprocessingConnector:

    def __init__(self):
        self.log = logger.get_logger(self.__class__.__name__)
        self.preprocessing_output_queue = Queue()
        self.preprocessing_input_queue = Queue()
        
        self.preprocessing_output_thread = threading.Thread(target=self._preprocessing_sending_thread)
        self.preprocessing_output_thread.start()
        
    def _preprocessing_sending_thread(self):
        self.log.info("Starting preprocessing sending thread")
        preprocessing_address = os.getenv('PREPROCESSING_SERVICE_HOST', 'localhost'), config.preprocessing_address()[1]
        self.log.info("Target address  = {}:{}".format(*preprocessing_address))
        while True:
            data = self.preprocessing_output_queue.get()
            sock = socket.socket()
            
            sock.connect(preprocessing_address)
            self.log.info("Connected to preprocessing on {}:{}".format(*preprocessing_address))
            self.log.info("Sending data to {}:{}".format(*preprocessing_address))
            utils.send(sock, data)
            resp = utils.receive(sock)
            self.preprocessing_input_queue.put(resp)
            sock.close()
    
    def send(self, proto_object):
        self.log.info("Sending PreprocessingRequest")
        self.preprocessing_output_queue.put(proto_object.SerializeToString())
    
    def receive(self):
        self.log.info("Waiting for data from Preprocessing")
        data = self.preprocessing_input_queue.get()
        self.log.info("Received data from Preprocessing")
        return data
    
    

