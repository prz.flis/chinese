


class config:
    __SERVER_HOST = ''
    __SERVER_PORT = 8700

    __PREPROCESSING_HOST = ''
    __PREPROCESSING_PORT = 8800

    __RECOGNITION_HOST = ''
    __RECOGNITION_PORT = 8900

    __TRANSLATE_HOST = ''
    __TRANSLATE_PORT = 8600

    __DB_CONTROLLER_HOST = ''
    __DB_CONTROLLER_PORT = 8500

    __REAL_MODULES = True

    __NUMEBER_OF_RECOGNIITON_INSTANCES = 1

    __NEURAL_NETWORK_MODEL_PATH = "models/pure_conv_lr_1e-4_vgg13_500class_batch_norm.hdf5"
    __NEURAL_NETWORK_LABELS_PATH = "models/labels.npy"

    # where image from client is stored
    __IMAGE_PATH = "./data/"

    #where to save numpy and char image files
    __SAVE_PATH = "./data/"

    @staticmethod
    def recognition_address():
        return (
            config.__RECOGNITION_HOST,
            config.__RECOGNITION_PORT
        )

    @staticmethod
    def server_address():
        return (
            config.__SERVER_HOST,
            config.__SERVER_PORT
        )

    @staticmethod
    def preprocessing_address():
        return (
            config.__PREPROCESSING_HOST,
            config.__PREPROCESSING_PORT
        )

    @staticmethod
    def use_real_modules():
        return config.__REAL_MODULES

    @staticmethod
    def save_path():
        return config.__SAVE_PATH

    @staticmethod
    def image_path():
        return config.__IMAGE_PATH

    @staticmethod
    def recognition_model():
        return (
            config.__NEURAL_NETWORK_MODEL_PATH,
            config.__NEURAL_NETWORK_LABELS_PATH
        )

    @staticmethod
    def number_of_recognition_instances():
        return config.__NUMEBER_OF_RECOGNIITON_INSTANCES

    @staticmethod
    def translate_address():
        return (
            config.__TRANSLATE_HOST,
            config.__TRANSLATE_PORT
        )

    @staticmethod
    def db_controller_address():
        return (
            config.__DB_CONTROLLER_HOST,
            config.__DB_CONTROLLER_PORT
        )

    @staticmethod
    def log_address():
        return (
            config.__LOG_HOST,
            config.__LOG_PORT
        )
