from multiprocessing import Process, Event
from configuration import config
import main
import preprocessing_instance
import recognition_instance
import translate_instance
import signal
import web_service
import db_controller_instance

def run_server():
    return Process(target=main.Server)

def run_preprocessing():
    return Process(target=preprocessing_instance.PreprocessingInstance)

def run_recognition(args):
    return Process(target=recognition_instance.RecognitionInstance, args=args)

def run_translating():
    return Process(target=translate_instance.TranslateInstance)

def run_db_controller():
    return Process(target=db_controller_instance.DbControllerInstance)

def run_web_service():
    return Process(target=web_service.main)

end_event = Event()

def signal_handler(signum, frame):
    print("Signal code: ", signum)
    end_event.set()

def setup_end_signal():
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGBREAK, signal_handler)
    signal.signal(signal.SIGABRT, signal_handler)

if __name__ == '__main__':
    # setup_end_signal()

    web_service = run_web_service()
    web_service.start()

    server = run_server()
    server.start()

    preprocessing = run_preprocessing()
    preprocessing.start()
    
    translating = run_translating()
    translating.start()

    db_controller = run_db_controller()
    db_controller.start()
    
    recognition_list = []
    host, port = config.recognition_address()
    for i in range(config.number_of_recognition_instances()):
        recognition_process = run_recognition((host, port+i))
        recognition_process.start()
        recognition_list.append(recognition_process)

    end_event.wait()

    server.join()
    preprocessing.join()
    translating.join()
    for item in recognition_list:
        item.join()
