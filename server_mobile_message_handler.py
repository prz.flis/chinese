import logger
import sys
import threading
import datetime
sys.path.append("Protocols/")
import hashlib
from configuration import config

import Server_Preprocessing_Protocol_pb2 as Server_Preprocessing
import Server_Recognition_Protocol_pb2 as Server_Recogniotion
import Mobile_Server_Protocol_pb2 as Mobile_Server

class ServerMobileMessageHandler:
    def __init__(self,
                 preprocessing_connector,
                 # recognition_connector,
                 recognition_traffic_router,
                 mobile_connector,
                 translate_connector,
                 db_controller_connector
                 ):
        
        self.log = logger.get_logger(self.__class__.__name__)
        
        self.preprocessing_connector = preprocessing_connector
        # self.recognition_connector = recognition_connector
        self.recognition_traffic_router = recognition_traffic_router
        self.mobile_connector = mobile_connector
        self.translate_connector = translate_connector
        self.db_controller_connector = db_controller_connector

        
        self._handlers = {
            'initRequest': self._handle_init,
            'image': self._handle_image,
            'translateRequest': self._handle_translate,
            'characterRateRequest': self._handle_char_rate
        }
        
        self.main_loop = threading.Thread(target=self._handler_loop_thread)
        self.main_loop.start()
    
    def _handler_loop_thread(self):
        while True:
            try:
                self.handle(self.mobile_connector.receive())
            except Exception as e:
                self.log.error("Exception: {}".format(e))

    def handle(self, args):
        conn, addr, data = args
        cmd = Mobile_Server.Command()
        cmd.ParseFromString(data)
        which_oneof = cmd.WhichOneof('type')
        self.log.info("Handling message {}".format(which_oneof))
        self._handlers[which_oneof](conn, addr, cmd)
    
    def _handle_translate(self, conn, addr, cmd):
        self.translate_connector.send(cmd.translateRequest)
        resp = Mobile_Server.TranslateResponse()
        resp.ParseFromString(self.translate_connector.receive())
        cmd = Mobile_Server.Command()
        cmd.translateResponse.CopyFrom(resp)
        self.mobile_connector.send(conn, addr, cmd)
    
    def _handle_char_rate(self, conn, addr, cmd):
        self.db_controller_connector.send(cmd.characterRateRequest)
        resp = Mobile_Server.CharacterRateResponse()
        resp.ParseFromString(self.db_controller_connector.receive())
        cmd = Mobile_Server.Command()
        cmd.characterRateResponse.CopyFrom(resp)
        self.log.info("characterRateResponse success ({})".format(cmd.characterRateResponse.success))
        self.mobile_connector.send(conn, addr, cmd)

    def _check_hash(self, data, img_hash):
        m = hashlib.new("md5")
        m.update(data)
        if img_hash == m.hexdigest():
            self.log.info("Hash is correct")
            return True
        else:
            self.log.warning("Hash is NOT CORRECT; {} != {}".format(img_hash, m.hexdigest()))
            return False

    def _get_unique_token(self):
        token = str(datetime.datetime.now()).replace('-', '_').replace(' ', '_').replace(':', '_').replace('.', '_')
        self.log.warning("TOKEN = {}".format(token))
        return token
    
    def _handle_init(self, conn, addr, req):
        response = Mobile_Server.InitResponse()
        response.success = True
        response.token = self._get_unique_token()
        command = Mobile_Server.Command()
        command.initResponse.CopyFrom(response)
        self.mobile_connector.send(conn, addr, command)

    def _handle_image(self, conn, addr, req):
        response = Mobile_Server.ImageResponse()
        hash_is_good = self._check_hash(req.image.image, req.image.hashCode)
        response.success = hash_is_good
        command = Mobile_Server.Command()
        command.imageResponse.CopyFrom(response)
        self.log.info("ImageResponse params: success = {}".format(hash_is_good))
        self.mobile_connector.send(conn, addr, command)

        if hash_is_good:
            filename = req.image.token + ".jpg"
            image_save_path = config.image_path() + filename
            # self.save_to_file(req.image.image, image_save_path)

            preprocessing_req = self.build_preprocessing_req(req, image_save_path)
            self.preprocessing_connector.send(preprocessing_req)

            preprocessing_resp = Server_Preprocessing.PreprocessingResponse()
            preprocessing_resp.ParseFromString(self.preprocessing_connector.receive())

            recognition_result = Mobile_Server.RecognitionResult()
            recog_req_to_send = []
            for preprocessed_char in preprocessing_resp.preprocessedCharacterList:
                recognition_req = Server_Recogniotion.RecognitionRequest()
                recognition_req.token = req.image.token
                recognition_req.preprocessedCharacter.CopyFrom(preprocessed_char)

                # self.recognition_connector.send(recognition_req.SerializeToString())
                recog_req_to_send.append(recognition_req.SerializeToString())

            self.recognition_traffic_router.send(recog_req_to_send)

            for recog_resp_received, preprocessed_char \
                    in zip(self.recognition_traffic_router.receive(),preprocessing_resp.preprocessedCharacterList):

                recognition_resp = Server_Recogniotion.RecognitionResponse()
                recognition_resp.ParseFromString(recog_resp_received)

                recog_char = recognition_result.character.add()
                recog_char.index = recognition_resp.recognizedCharacter.index
                recog_char.character = recognition_resp.recognizedCharacter.character
                recog_char.certainty = recognition_resp.recognizedCharacter.certainty
                recog_char.characterRectangle.CopyFrom(preprocessed_char.rectangle)

            command = Mobile_Server.Command()
            command.recognitionResult.CopyFrom(recognition_result)
            self.mobile_connector.send(conn, addr, command)

            self.log.info("Recognition process completed")

    def build_preprocessing_req(self, image_req, image_save_path):
        preprocessing_req = Server_Preprocessing.PreprocessingRequest()
        preprocessing_req.token = image_req.image.token
        # preprocessing_req.imagePath = image_save_path
        preprocessing_req.imageBinary = image_req.image.image
        preprocessing_req.savePath = config.save_path()
        preprocessing_req.recognitionRectangle.CopyFrom(image_req.image.recognitionRectangle)
        self.log.info("PreprocessingReq params: token = {};"
                      " imagePath = {};"
                      # " savePath = {};"
                      " rectangle = {} {}"
                      .format(
                                preprocessing_req.token,
                                  # preprocessing_req.imagePath,
                                  preprocessing_req.savePath,
                                  (
                                    preprocessing_req.recognitionRectangle.x1,
                                    preprocessing_req.recognitionRectangle.y1,
                                  ),
                                  (
                                    preprocessing_req.recognitionRectangle.x2,
                                    preprocessing_req.recognitionRectangle.y2,
                                  ),
                              ))
        return preprocessing_req


        
        

