import unittest
import socket
import sys

sys.path.append("Protocols/")

import Server_Recognition_Protocol_pb2 as Server_Recognition

from configuration import config
import utils

class TestRecognitionInstance(unittest.TestCase):

    def setUp(self):
        self.server_socket = socket.socket()
        self.server_socket.connect(('localhost',
                                   config.recognition_address()[1]))

    def tearDown(self):
        self.server_socket.close()

    def _build_recognition_req(self, token,index, characterNumpyObjectPath, characterImagePath):
        req = Server_Recognition.RecognitionRequest()
        req.token = token
        req.preprocessedCharacter.index = index
        req.preprocessedCharacter.characterNumpyObjectPath = characterNumpyObjectPath
        req.preprocessedCharacter.characterImagePath = characterImagePath

        req.preprocessedCharacter.rectangle.x1 = 1
        req.preprocessedCharacter.rectangle.x2 = 2
        req.preprocessedCharacter.rectangle.y1 = 3
        req.preprocessedCharacter.rectangle.y2 = 4

        return req

    def serialize_resp(self, data):
        resp = Server_Recognition.RecognitionResponse()
        resp.ParseFromString(data)
        return resp

    def check_resp(self, to_check, resp):
        token, index = to_check
        self.assertEqual(token, resp.token)
        self.assertEqual(index, resp.recognizedCharacter.index)

    def test_recognition_req_resp(self):
        for i in range(1, 10):
            #build req
            index = 42
            token = "some_token"
            numpy_path = "some_token_{}.npy".format(i)
            img_path = "imgpath"
            req = self._build_recognition_req(token, index, numpy_path, img_path)
            # send it
            utils.send(self.server_socket, req.SerializeToString() )
            # self.server_socket.send()
            # receive resp
            data = utils.receive(self.server_socket)
            # data = self.server_socket.recv(1023000)
            resp = self.serialize_resp(data)
            #check
            to_check = (token, index, )
            self.check_resp(to_check, resp)


if __name__ == '__main__':
    unittest.main()
