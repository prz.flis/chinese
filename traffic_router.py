from multiprocessing import Queue
import threading
from configuration import config
from recognition_connector import RecognitionConnector
from concurrent.futures import ThreadPoolExecutor
import os


class TrafficRouter:

    def __init__(self, num_of_instances):
        self.num_of_instances = num_of_instances
        self.input_queue = Queue()
        self.output_queue = Queue()
        self._connectors = [
            RecognitionConnector((os.getenv('RECOGNITION_SERVICE_HOST', 'localhost'), config.recognition_address()[1]+i)) for i in range(num_of_instances)
        ]
        self._routing_thread = threading.Thread(target=self._routing)
        self._routing_thread.start()

    def _routing(self):
        i = 0
        thread_pool = ThreadPoolExecutor(max_workers=50)
        while True:
            list_of_data = self.input_queue.get()
            futures = []
            for data in list_of_data:
                connector = self._connectors[i % self.num_of_instances]
                connector.send(data)
                futures.append(thread_pool.submit(connector.receive))
                i += 1

            self.output_queue.put([future.result() for future in futures])
            i = 0

    def send(self, list_of_data):
        self.input_queue.put(list_of_data)

    def receive(self):
        return self.output_queue.get()

