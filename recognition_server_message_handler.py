import logger
import sys
import threading
import io
import utils
from configuration import config
if config.use_real_modules():
    sys.path.append("externals/neural_networks_chinese_char_recog")
    from ConvNet import ConvNet
    import numpy as np

sys.path.append("Protocols/")
import Server_Recognition_Protocol_pb2 as Server_Recogniotion

class RecognitionServerMessageHandler:

    def __init__(self, server_connector):

        self.log = logger.get_logger(self.__class__.__name__+"_{}".format(config.recognition_address()[1]))

        if config.use_real_modules():
            self.log.info("Loading recognition")
            self.recognition = ConvNet(*config.recognition_model())
            for i in range(5):
                self.log.info("Loading completed !!!")

        self.server_connector = server_connector

        self.main_loop_thread = threading.Thread(target=self._main_loop)
        self.main_loop_thread.start()

    def _main_loop(self):
        while True:
            self._handle_recognition_request(self.server_connector.receive())

    def _handle_recognition_request(self, args):
        conn, addr, data = args
        req = Server_Recogniotion.RecognitionRequest()
        req.ParseFromString(data)
        resp = Server_Recogniotion.RecognitionResponse()
        self.log.info("Request params: token({})\t"
                      "index({})\t"
                      # "numpy_path({})\t"
                      # "img_path({})\t"
                      "rectangle {}, {}\t".format(
            req.token,
            req.preprocessedCharacter.index,
            # req.preprocessedCharacter.characterNumpyObjectPath,
            # req.preprocessedCharacter.characterImagePath,
            (
                req.preprocessedCharacter.rectangle.x1,
                req.preprocessedCharacter.rectangle.y1
            ),
            (
                req.preprocessedCharacter.rectangle.x2,
                req.preprocessedCharacter.rectangle.y2
            ),
        ))
        if config.use_real_modules():
            self._do_real_recognition(req, resp)
        else:
            self._do_fake_recognition(req, resp)
        self.server_connector.send(conn, addr, resp)

    def _do_real_recognition(self, req, resp):
        self.log.info("Do real recognition....")
        resp.token = req.token
        resp.recognizedCharacter.index = req.preprocessedCharacter.index
        # path = req.preprocessedCharacter.characterNumpyObjectPath

        bio = io.BytesIO(req.preprocessedCharacter.characterNumpyObjectBinary)
        binary = np.load(bio)
        self.log.info("Starting prediction...")
        char, certainty = self.recognition.predict(binary)
        self.log.info("Prediction completed !!!")
        resp.recognizedCharacter.character = char
        resp.recognizedCharacter.certainty = int(certainty)
        self.log.info("Response params: token({})\t"
                      "index({})\t"
                      "char({})\t"
                      "certainty({})\t"
                      "".format(
            resp.token,
            resp.recognizedCharacter.index,
            resp.recognizedCharacter.character,
            resp.recognizedCharacter.certainty
        ))

    def _do_fake_recognition(self, req, resp):
        resp.token = req.token
        resp.recognizedCharacter.index = req.preprocessedCharacter.index
        resp.recognizedCharacter.character = "B6:F6"
        resp.recognizedCharacter.certainty = 42

        self.log.info("Response params: token({})\t"
                      "index({})\t"
                     # "char({})\t"
                      "certainty({})\t"
                      "".format(
            resp.token,
            resp.recognizedCharacter.index,
           # resp.recognizedCharacter.character,
            resp.recognizedCharacter.certainty
        ))






