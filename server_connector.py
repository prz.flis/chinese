import socket
import threading
from multiprocessing import Queue
import utils
import logger

class ServerConnector:

    def __init__(self, address):
        host, port = address
        self.log = logger.get_logger(self.__class__.__name__ + "_{}".format(port))
        self.endpoint_to_server = self.setup_endpoint_to_server(host, port)

        self.server_input_queue = Queue()
        self.server_output_queue = Queue()

        self.server_output_thread = threading.Thread(target=self._server_sending_thread)
        self.server_output_thread.start()

        self.server_input_thread = threading.Thread(target=self._connection_with_server)
        self.server_input_thread.start()

    def setup_endpoint_to_server(self, host, port):
        soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        soc.bind((host, port))
        soc.listen(1)
        return soc

    def _server_sending_thread(self):
        self.log.info("Starting server sending thread")
        while True:
            conn, addr, data = self.server_output_queue.get()
            self.log.info("Sending data to {}:{}".format(*addr))
            utils.send(conn, data)

    def send(self, conn, addr, command):
        self.log.info("Sending message to server")
        self.server_output_queue.put((conn, addr, command.SerializeToString()))

    def receive(self):
        data = self.server_input_queue.get()
        self.log.info("Received data from server")
        return data

    def _connection_with_server(self):
        while True:
            conn, addr = self.endpoint_to_server.accept()
            self.log.info("Starting connection with  {}:{}".format(*addr))
            while True:
                data = utils.receive(conn)
                if data:
                    self.server_input_queue.put((conn, addr, data))
                else:
                    self.log.info('Disconnected with {}:{}'.format(*addr))
                    conn.close()
                    break


