import logger
from server_connector import ServerConnector
from preprocessing_server_message_handler import PreprocessingServerMessageHandler

from configuration import config

class PreprocessingInstance:

    def __init__(self):
        host, port = config.preprocessing_address()
        self.log = logger.get_logger(self.__class__.__name__ + '_{}'.format(port))
        self.log.info("===== STARTING PREPROCESSING INSTANCE =====")
        self.server_connector = ServerConnector((host, port))

        self.preprocessing_server_message_handler = PreprocessingServerMessageHandler(
            server_connector=self.server_connector,
        )


if __name__ == '__main__':
    PreprocessingInstance()
